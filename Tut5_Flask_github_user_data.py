from flask import Flask
from githubdata.views import user_blueprint

app = Flask(__name__)


# @app.route('/')
# def hello_world():
#     return 'Hello World!'


@app.route('/githubdata')
def githubdata():
    return 'Hello World'


@app.route('/githubdata/test')
def test():
    return 'My second view!'


app.register_blueprint(user_blueprint, url_prefix='/githubdata')


# @app.route('/githubdata/profile', methods=['GET', 'POST'])
# def profile():
#     parsedData = []
#     if request.method == 'POST':
#         # username = request.values['user']
#         form = request.form
#         username = request.values.get('user')
#         req = requests.get('https://api.github.com/users/' + username)
#         jsonList = []
#         jsonList.append(json.loads(req.text))
#         print('list:', jsonList)
#         userData = {}
#         for data in jsonList:
#             userData['name'] = data['name']
#             userData['blog'] = data['blog']
#             userData['email'] = data['email']
#             userData['public_gists'] = data['public_gists']
#             userData['public_repos'] = data['public_repos']
#             userData['avatar_url'] = data['avatar_url']
#             userData['followers'] = data['followers']
#             userData['following'] = data['following']
#         parsedData.append(userData)
#     return render_template('profile.html', data=parsedData)


if __name__ == '__main__':
    app.run()
