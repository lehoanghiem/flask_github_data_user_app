from flask import render_template, request
import requests
import json
from flask import Blueprint


user_blueprint = Blueprint('user_blueprint', __name__)


@user_blueprint.route('/profile', methods=['GET', 'POST'])
def profile():
    parsedData = []
    if request.method == 'POST':
        # username = request.values['user']
        form = request.form
        username = request.values.get('user')
        req = requests.get('https://api.github.com/users/' + username)
        jsonList = []
        jsonList.append(json.loads(req.text))
        print('list:', jsonList)
        userData = {}
        for data in jsonList:
            userData['name'] = data['name']
            userData['blog'] = data['blog']
            userData['email'] = data['email']
            userData['public_gists'] = data['public_gists']
            userData['public_repos'] = data['public_repos']
            userData['avatar_url'] = data['avatar_url']
            userData['followers'] = data['followers']
            userData['following'] = data['following']
        parsedData.append(userData)
    return render_template('profile.html', data=parsedData)
